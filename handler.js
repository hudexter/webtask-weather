'use latest';

import express from 'express';
import { fromExpress } from 'webtask-tools';
import bodyParser from 'body-parser';
const app = express();

import Nexmo from 'nexmo';
import pug from 'pug';

import geocoder from 'geocoder';
import request from 'request';

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// define a route for returning the HTML page
app.get('/', (req, res) => {
  res.send(renderView());
});

// define a route for sending the SMS message
app.post('/send', (req,res) => {
  var ctx = req.webtaskContext;
  var secrets = ctx.secrets;
  var api_key = secrets.nexmo_api_key;
  var api_secret = secrets.nexmo_api_secret;
  var from = secrets.nexmo_from;
  var weather_key = secrets.openweathermap_key;
  // initialize Nexmo
  var nexmo = new Nexmo({
      apiKey: api_key,
      apiSecret: api_secret,
    });

  // output the posted body to the logs
  console.log(req.body);
  
  geocoder.reverseGeocode( req.body.latitude, req.body.longitude, function ( err, data ) {

  console.log(data);  
  var city = data.results[0].formatted_address;
  var lat = req.body.latitude;
  var lon = req.body.longitude;
  var weather_key = secrets.openweathermap_key;
  var weather_url = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=imperial&appid=${weather_key}`;
  console.log(city);
  console.log(weather_url);
  request(weather_url, function (err, response, weatherbody) {
      if(err){
        console.log('error:', error);
      } else {

      }
      let weather = JSON.parse(weatherbody)
        console.log(weather);
        let message = `It's ${weather.weather[0].description} and ${weather.main.temp} degrees (F) in ${weather.name}!`;
        
        // send the message
      req.body.message = "You are at " + city + ".\n" + message; 
      
      console.log(req.body.message);
      if(req.body.to.startsWith("1")===false){
        req.body.to = "1" + req.body.to;
      }
      nexmo.message.sendSms(from, req.body.to, req.body.message, (err, data) => {
        const status = err ? 400 : 200;
        const message = err ? err.message : 'Sent!';
        res.writeHead(status, { 'Content-Type': 'text/html' });
        return res.end('<h1>' + message + '</h1>');
      });
  
    });
          
  });


});

// the page
var page=`
head
  title My Site
  script.
    var x = document.getElementById("demo");
    var globalposition = null;
    function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
    }
    function showPosition(position) {
        globalposition = position.coords;
        console.log(position.coords);
   
        // x.innerHTML = "Latitude: " + position.coords.latitude + 
        // "<br>Longitude: " + position.coords.longitude;
        
        document.getElementById("longitude").defaultValue = position.coords.longitude;
        document.getElementById("latitude").defaultValue = position.coords.latitude;
    }
  
  
p Send local weather information to your phone

p Click bellow button for your browser location.
button#demo(onclick='getLocation()') Click

form(action='/weather/send' method='POST' enctype='application/x-www-form-urlencoded')
  table
    tr
    tr
      td 
        p Mobile number (US/Canada only)
      td 
        input(name='to' type='text', id="to")
    tr
    tr
      td 
        p Latitude
      td 
        input(type='text', id='latitude', name='latitude')
    tr
    tr
      td 
        p Longitude
      td 
        input(type='text', id='longitude', name = 'longitude')
    tr
      td(colspan='2')
        input(type='submit' text='Send')
    tr`;

// render the page using pug
function renderView() {
  return pug.render(page);
}

module.exports = fromExpress(app); 
